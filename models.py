from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from config import create_app 

app = create_app()
db = SQLAlchemy(app)
 
class ClusterModel(db.Model):
    __tablename__ = 'clusters'
 
    id = db.Column(db.Integer(), primary_key = True)
    initial = db.Column(JSON)
    final = db.Column(JSON)
 
    def __init__(self, initial, final):
        self.initial = initial
        self.final = final
 
    def __repr__(self):
        return f"<statement>"