from functools import wraps
from flask import Flask, request
from algo import App
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import create_app
from models import db, ClusterModel
import json
from middlewares.ValidateData import ValidateData

'''app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://postgres:postgres@localhost:5432/algodb"
#app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
migrate = Migrate(app, db)'''

app = create_app()
db = SQLAlchemy(app)


@app.route("/algo", methods=['POST'])
@ValidateData()
def reclustering():
    input = request.json
    
    app = App()
    app.add_init_input(input)
    final = app.run(2,4)
    
    clm = ClusterModel(json.dumps(input), json.dumps(final))
    db.session.add(clm)
    db.session.commit()
    
    return final


if __name__ == '__main__':
    app.run(debug=False)