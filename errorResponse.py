from enum import Enum

class ErrResponse(Enum):
    ValidationError = "ValidationError"