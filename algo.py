from fileinput import close
from mimetypes import init
import ast
from tokenize import group
from typing import List
from xxlimited import new


'''
  Solving the problem with the K-means algorithm
'''

class App:
  
  init_input = {}
  N = 0
  K = 0
  D = 0
  
  def add_init_input(self, input):
    self.init_input = input
  
  def sum_of_groups(self):
    'Get the total amount of groups in the input'
    _N = 0
    for g in self.init_input:
      _N += 1

    return _N
  
  # HIBA: ROSSZUL ADJA VISSZA A VEKTOR LISTÁT.
  def get_vectors(self):
    'Get the pure vectors of each group and put it into a list and return that'
    
    temp = []
    
    for group in self.init_input.values():
      for vec in group:
        if isinstance(vec, float):
          temp.append(group)
          break
        else:
          temp.append(vec)

    print("GET_VECTOR:", temp)
    return temp
  
  
  def rejoin(self, closest, averages):
  
    new = {}

    checked_indices = []
    checked_cluster_indices = []
    for i, group in enumerate(closest):
      if i+1 in checked_indices: 
        continue
      if closest[group] == True:
        if i+1 not in checked_cluster_indices:
        # if it's True, it means that it belongs to itself
          new[group] = self.init_input[group]
      else:
        
        '''
          otherwise, find the index of the value it has (a list of numbers), 
          which means, it's supposed to belong to that group, do the join.
          
          Add to checked_indices the index to avoid double join for the same group, it's already been joined.
        '''
        cluster_index = averages.index(closest[group])+1
        if cluster_index in checked_cluster_indices:
          _key = ""
          for key in new:
            if str(cluster_index) in key:
              _key = key
              break
 
          val = new[_key]
          val += self.init_input[group]
          new["{}_{}".format(key, group[-1])] = val
          del(new[_key])
          checked_cluster_indices.append(cluster_index) 
          checked_indices.append(i+1)
          continue
          
        print("GXX:", "{}_{}".format(group, cluster_index))
        new["{}_{}".format(group, cluster_index)] = []

        for g in self.init_input:
          if g == "group_{}".format(i+1):
            vs = self.init_input[g] # vs means vectors
            for v in vs:
              new["{}_{}".format(group, cluster_index)].append(v)
              
          elif g == "group_{}".format(cluster_index):
            vs = self.init_input[g]
            for v in vs:
              new["{}_{}".format(group, cluster_index)].append(v)
        
        checked_cluster_indices.append(cluster_index) 
        
      checked_indices.append(i+1)
        
      
    # print("REJOINED:", new) {'group_1': [[0.1, 0.4, -0.7, 0.8], [-0.1, 0.0, -0.1, -0.4]], 'group_2_3': [[-0.7, -0.8, -0.6, -0.5], [-0.5, -0.9, -0.3, -0.2], [-0.2, 0.2, -0.1, 0.1]]}
    return new
  
  def get_average(self,group):
    'Get the avarage of each cluster (group). Group = list of vectors.'
    
    temp = [0] * self.D
    length = len(group)
    
    for i, vector in enumerate(group):
      if isinstance(vector, float):
        #If True, group is a single list of numbers, where 'vector' is a float number, so add it to the 'temp' immediately.
        temp[i] += vector
      else:
        for index, number in enumerate(vector):
          temp[index] += number
    
    for ix, val in enumerate(temp):
      temp[ix] = round(val / length, 2)
    
    return temp

  def get_distances(self, vectors, averages):
    'Get the vector and calculate the distances for each "average"'
    temp = {}

    for avg in averages:
      dist = 0
      for vec in vectors:
        for vec_num in vec:
          for avg_num in avg:
            dist += round(abs(avg_num-vec_num),2)
            
        if str(vec) not in temp:
          temp[str(vec)] = [{
            str(avg) : round(dist, 2)
          }]
        else:
          temp[str(vec)].append({
            str(avg) : round(dist, 2)
          })
    
    #print("DISTANCES:",temp)
    return temp
  
  def get_closest(self, distances):
  
    temp = {}
    
    for vector in distances:
      #print(distances[vector])
      key = ""
      val = 0
      for i, dt in enumerate(distances[vector]): # i == index, dt == dictionary
        #print(i, dt)
        for key2 in dt:
          #print(key2)
          if i == 0:
            key = key2
            val += dt[key2]
          else:
            if dt[key2] < val:
              val = dt[key2]
              key = key2
              
      temp[str(vector)] = str(key)

    print("CLOSEST:",temp) # e.g.: {'[0.1, 0.4, -0.7, 0.8]': '[0.0, 0.2, -0.4, 0.2]', '[-0.1, 0.0, -0.1, -0.4]': '[0.0, 0.2, -0.4, 0.2]', '[-0.7, -0.8, -0.6, -0.5]': '[-0.35, -0.35, -0.2, -0.05]', '[-0.5, -0.9, -0.3, -0.2]': '[-0.35, -0.35, -0.2, -0.05]', '[-0.2, 0.2, -0.1, 0.1]': '[-0.35, -0.35, -0.2, -0.05]'}
    return temp
  
      
  def remove_nesting(self, new_input):
    temp = {}

    for group in new_input:
      #print("G:", new_input[group])
      for v in new_input[group]:
        vectors = []
        
        if isinstance(v, list):
          for x in v:
            vectors.append(x)
        else:
          vectors.append(v)

        if group in temp:
          temp[group].append(vectors)
        else:
          temp[group] = [vectors]
  
    print("REMOVED:",temp)
    return temp
  
  def get_distanceOf_two_vectors(self, vector_one, vector_two):
    
    dist = 0
    for v in vector_two:
      for item in vector_one:
        dist += round(abs(v-item), 2)
    
    return dist
  
  
  def make_decision(self, new_groups, averages):
  
    closest = {}
    
    for index, group in enumerate(new_groups):
      ngr = new_groups[group]
      _dist = 0
      for index2, dict in enumerate(ngr):
        d = 0
        k = ""
        for key in dict:
          k = ast.literal_eval(key)
          if dict[key] == True:
            d += self.get_distanceOf_two_vectors(k,averages[index])
          else:
            d += self.get_distanceOf_two_vectors(k, dict[key])
          
          if index2 == 0:
            _dist += d
          
          if d < _dist:
            _dist = d
      
      closest[group] = dict[key]
      
    #print(closest)  .e.g.: {'group_1': [-0.35, -0.35, -0.2, -0.05], 'group_2': True, 'group_3': [0.0, 0.2, -0.4, 0.2]}

    return closest
  
  
  def clustering(self, only_vectors):
    # 1. get the averages of each group
    averages = [0] * self.N
    for ix, group in enumerate(self.init_input.values()):
      # print(group)  e.g.: group == [[-0.5, -0.9, -0.3, -0.2], [-0.2, 0.2, -0.1, 0.1]]
      averages[ix] = self.get_average(group)
    
    print("AVERAGES:", averages)
    # 2. get distances for the new centroids
    distances = self.get_distances(only_vectors, averages)
    
    # 3. get the closest one
    closest = self.get_closest(distances)
    #print("CLOSEST:", closest)
    
    # 4. check if the key has a closest average that is different from its group and create a new group variable
    # where: key = vector (embedding), value = True, if it can stay in the same group, False, if it can't
    new_groups = {}
    for key in closest:
      val = ast.literal_eval(closest[key])
      for index, group in enumerate(self.init_input):
        group_of_average = averages.index(val)
        #print(key, init_input[group])
        if ast.literal_eval(key) in self.init_input[group]: 
          if group_of_average == index: # included and the index matches
            #print(True, key, val)
            if group in new_groups:
              new_groups[group].append({ str(key) : True })
            else:
              new_groups[group] = [{ str(key) : True }]
          else:
            if group in new_groups:
              new_groups[group].append({ str(key) : val })
            else:
              new_groups[group] = [{ str(key) : val }]

    print("NEW GROUPS:", new_groups) # e.g.: {'group_1': [{'[0.1, 0.4, -0.7, 0.8]': True}, {'[-0.1, 0.0, -0.1, -0.4]': True}], 'group_2': [{'[-0.7, -0.8, -0.6, -0.5]': [-0.35, -0.35, -0.2, -0.05]}], 'group_3': [{'[-0.5, -0.9, -0.3, -0.2]': True}, {'[-0.2, 0.2, -0.1, 0.1]': True}]}
    
    new_closest = self.make_decision(new_groups, averages)
    print("NEW CLOSEST:", new_closest)
    new_input = self.rejoin(new_closest, averages) # e.g.: {'group_1_3': [[[0.1, 0.4, -0.7, 0.8], [-0.1, 0.0, -0.1, -0.4]], [[-0.5, -0.9, -0.3, -0.2], [-0.2, 0.2, -0.1, 0.1]]], 'group_2': [[-0.7, -0.8, -0.6, -0.5]]}
    print("NEW INPUT:", new_input)
    final_new_input = self.remove_nesting(new_input) # e.g.: {'group_1_3': [[0.1, 0.4, -0.7, 0.8], [-0.1, 0.0, -0.1, -0.4], [-0.5, -0.9, -0.3, -0.2], [-0.2, 0.2, -0.1, 0.1]], 'group_2': [-0.7, -0.8, -0.6, -0.5]}
    #print("FINAL NEW INPUT:", final_new_input)
    return final_new_input
  
  def run(self, K, D):
    self.N = self.sum_of_groups()
    self.K = K
    self.D = D
    
    _old = {}
    
    while _old != self.init_input:
      print("INIT INPUT:", self.init_input)
      self.N = self.sum_of_groups()
      vectors = self.get_vectors()
      new = self.clustering(vectors)
      _old = self.init_input
      self.init_input = new
      
    print("DONE")
    print("FINAL: ", self.init_input)
    return self.init_input
    
# To start the app
def main():
  
  '''
    The example input from the exercise
  '''
  init_input = {
    "group_1": [
    [0.1,0.4,-0.7,0.8], 
    [-0.1,0.0,-0.1,-0.4]
    ], 
    "group_2": [
      [-0.7, -0.8, -0.6, -0.5]
    ], 
    "group_3": [
      [-0.5, -0.9, -0.3, -0.2],
      [-0.2, 0.2, -0.1, 0.1]
    ]
  }
  
  app = App()
  app.add_init_input(init_input)
  
  app.run(2,4)
  

if __name__ == "__main__":
    main()
    

'''
  EXAMPLE OUTPUT FOR MY CODE:
  
    Initial value:
      init_input: {
        "group_1": [
        [0.1,0.4,-0.7,0.8], 
        [-0.1,0.0,-0.1,-0.4]
        ], 
        "group_2": [
          [-0.7, -0.8, -0.6, -0.5]
        ], 
        "group_3": [
          [-0.5, -0.9, -0.3, -0.2],
          [-0.2, 0.2, -0.1, 0.1]
        ]
      }
    
    Final:
      {
        'group_1': [[0.1, 0.4, -0.7, 0.8], [-0.1, 0.0, -0.1, -0.4]], 
        'group_2_3': [[-0.7, -0.8, -0.6, -0.5], [-0.5, -0.9, -0.3, -0.2], [-0.2, 0.2, -0.1, 0.1]]}
      }
'''
