from functools import wraps
from flask import Flask, request
from errorResponse import ErrResponse

def validateNames(json):
    for index, group in enumerate(json):
        if "{}_{}".format("group", index+1) == str(group):
            continue
        else:
            return False
    return True

def ValidateData():
    def _home_decorator(f):
        @wraps(f)
        def __home_decorator(*args, **kwargs):
            if not validateNames(request.json):
                return {
                    "error": ErrResponse.ValidationError.name
                }
                
            result = f(*args, **kwargs)
            return result
        return __home_decorator
    return _home_decorator